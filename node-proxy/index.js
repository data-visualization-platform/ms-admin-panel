const express       = require('express');
const yellow        = '\x1b[33m%s\x1b[0m';
const kafka         = require("./kafka-consumer.js");
const bodyParser    = require('body-parser');
const pino          = require('express-pino-logger')();
const WSServer      = require('ws').Server;


// Create express server
var app = express();
app.onRemote = (process.argv.slice(2)[0] == 'production');

// CORS
console.log(yellow, "Setting up cors.");
app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();        
});
app.enable('trust proxy');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(pino);
app.on('request', app);


// Read .env file
console.log(yellow, "Reading environment variables.");
const dotenv = require('dotenv');
dotenv.config();

// Server
var server = app.listen(process.env.PROXY_PORT || 2999, () =>
  console.log(yellow, "Proxy Server started @ " + server.address().port)
);

// Create Socker Server
const wss = new WSServer({ server: server });

// On Client Connection
wss.on('connection', (ws) => {
    ws.isAlive = true;
    ws.on('pong', () => {
        ws.isAlive = true;
    });
    console.log("User connected!");
});

// Handle broken connections
setInterval(() => {
    wss.clients.forEach((ws) => {       
        if (!ws.isAlive) return ws.terminate();       
        ws.isAlive = false;
        ws.ping(null, false, true);
    });
}, 10000);

// Start Kafka Consumer
console.log(yellow, "Starting kafka-consumer.");
kafka.connect("Upload_Kafka_Topic");
kafka.consume((message) => {
    // Inform all connected clients of Kafka Message
    // TODO: how to do delegation if multiple client servers exist?
    wss.clients.forEach(client => { client.send(message); });
});

// Simple Status Message
app.get('/api/status', (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify({ status: `Proxy Server Running!` }));
});
