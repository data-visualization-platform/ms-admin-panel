const Kafka = require('node-rdkafka');


module.exports = {

	stream: null,

  	connect:(topic) => {
		console.log(`Broker addresses are: ${process.env.BROKERS_IP_ADDRESSES}`);

		this.stream =  Kafka.KafkaConsumer.createReadStream({
		  'metadata.broker.list': process.env.BROKERS_IP_ADDRESSES,
		  'group.id': 'upload-group',
		  'socket.keepalive.enable': true
		}, {}, {
		  topics: topic,
		  waitInterval: 0,
		  objectMode: false
		});
  	},

  	consume:(callback, parseAsJson=true) => {
		this.stream.on('data', (message) => {
			var debuffered = message.toString();
	  		callback(parseAsJson 
	  			? debuffered 
	  			: JSON.parse(debuffered));
		});
		this.stream.on('error', function(err) {
		  if (err) console.log(err);
		});
		this.stream.consumer.on('event.error', function(err) {
		  console.log(err);
		})
  	}
};