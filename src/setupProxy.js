let { createProxyMiddleware } = require('http-proxy-middleware');
if (typeof createProxyMiddleware != 'function') {
    createProxyMiddleware = require('http-proxy-middleware');
}


module.exports = function(app) {
    app.use(createProxyMiddleware('/api', { 
        target: `http://localhost:${process.env.PROXY_PORT}` 
    }));
    app.use('/ws', createProxyMiddleware('/ws', {
        target: `ws://localhost:${process.env.PROXY_PORT}`,
        changeOrigin: true,
        ws: true,
        secure: false,
        logLevel: 'debug'
    }));
    console.log(`Proxy on => localhost:${process.env.PROXY_PORT}`);
};