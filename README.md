# Admin Panel

### Local Setup

``` bash
# Eventually change node modules permission level
sudo chmod -R 755 ./node_modules
sudo chown -R $USER ./node_modules/

# go into app's directory
$ cd my-project

# install app's dependencies
$ npm install
```

```bash
# build for production with minification
$ npm run build
```

``` bash
# dev server with hot reload at http://localhost:3000
$ npm start
```

### Docker Setup

```
sudo docker-compose build
sudo docker-compose up -d
```

### Kafka Proxy Setup

```
cd ./node-proxy
node proxy.js
```

Ensure /node-proxy/.env contains correct broker addresses, as this is not auto generated, yet.
/node-proxy/.env and ./.env can be merged later as soon as consumption works.