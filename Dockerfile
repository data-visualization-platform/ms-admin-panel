FROM node:8.13-alpine
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
# set environment variables
ARG PROXY_PORT
ARG PORT
ARG CHOKIDAR_USEPOLLING
ARG BROKERS_IP_ADDRESSES
ENV PROXY_PORT=$PROXY_PORT
ENV PORT=$PORT
ENV CHOKIDAR_USEPOLLING=$CHOKIDAR_USEPOLLING
ENV BROKERS_IP_ADDRESSES=$BROKERS_IP_ADDRESSES
# copy package.json
COPY package.json ./
COPY package-lock.json ./
RUN apk --no-cache add \
      bash \
      libc6-compat \
      g++ \
      ca-certificates \
      lz4-dev \
      musl-dev \
      cyrus-sasl-dev \
      openssl-dev \
      make \
      python
RUN apk add --no-cache --virtual .build-deps gcc zlib-dev libc-dev bsd-compat-headers py-setuptools bash
RUN npm install --silent
# for some reason this hacky solution needs to recompile its dependencies to work...
RUN npm rebuild
COPY . ./
CMD ["npm", "run", "dev"]
